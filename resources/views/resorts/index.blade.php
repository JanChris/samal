@extends('layout')
@section('title', 'Resort')

@section('content')
    <h4>Resorts</h4>
    <a class="btn btn-sm btn-info my-2" href="{{ route('resorts.create') }}">Add Resort</a>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
        @if (count($resorts))
            <table class="table table-light table-bordered table-striped">
                <thead>
                <tr>
                    <th>Resort Name</th>
                    <th>Contact No.</th>
                    <th>Location</th>
                    <th>Category</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($resorts as $resort)
                    <tr>
                        <td>{{ $resort->name }}</td>
                        <td>{{ $resort->contact }}</td>
                        <td>{{ $resort->location }}</td>
                        <td>{{ $resort->category }}</td>
                        <td>
                            @csrf
                            <a class="btn btn-sm btn-success" href="{{ route('resorts.show', $resort->id) }}">View</a>
                            <a class="btn btn-sm btn-primary" href="{{ route('resorts.edit', $resort->id) }}">Edit</a>
                            <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $resort->id }}">Delete</button>
                            {{--delete modal--}}
                            <div class="modal" id="delete{{ $resort->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title text-center">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p>Are you sure you want to delete?</p>

                                            <form class="d-inline-block" method="post" action="{{ route('resorts.destroy', $resort->id) }}">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $resorts->links() }}
        @else
            <div class="" align="center">
                <h4>No data!</h4>
            </div>
        @endif
    </div>
@stop
