@extends('layout')

@section('title', 'Add Resort')

@section('content')
    <h2 class="my-3">Add Resort</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('resorts.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="contact">Contact No.</label>
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact No.">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="location">location</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Location">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="category">Category</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Category">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="amenity">Amenity</label>
                    <input type="text" class="form-control" id="amenity" name="amenity" placeholder="Amenity">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Description" rows="10"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Submit</button>
        </div>
    </form>
@stop
