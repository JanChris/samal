@extends('layout')

@section('title', 'Add Event')

@section('content')
    <h2 class="my-3">Add Event</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('events.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Event Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Event Name">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="location">Event location</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Event location">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label for="date">Date</label>
                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                        <input type="text" id="date" name="date" placeholder="M/dd/YYYY H:mm" class="form-control datetimepicker-input" data-target="#datetimepicker1"/>
                        <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="detail">Event Details</label>
            <textarea type="text" class="form-control" id="detail" name="detail" placeholder="Event Details" rows="10"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Submit</button>
        </div>
    </form>
@stop

@section('script')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>
@stop
