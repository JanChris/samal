@extends('layout')

@section('title', 'Event List')

@section('content')
    <h4>Events {{ $event->name }}</h4>
    <a class="btn btn-sm btn-outline-info" href="{{ route('events.edit', $event->id) }}">Edit</a>
    <button class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">Delete</button>
    <hr>
    <div class="my-2">
        <div class="mb-3">
            <i class="fa fa-pin"></i>
            {{ $event->location }}
        </div>
        <div class="card-title">
            <h5>Details</h5>
        </div>
        <div class="card-body">
            {{ $event->detail }}
        </div>
    </div>

    {{--delete modal--}}
    <div class="modal" id="delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title text-center">Delete Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <p>Are you sure you want to delete?</p>

                    <form class="d-inline-block" method="post" action="{{ route('events.destroy', $event->id) }}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
