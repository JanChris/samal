@extends('layout')

@section('title', 'Edit Hotline')

@section('content')
    <h2 class="my-3">Update Hotline</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('hotlines.update', $hotline->id) }}" method="post">
        @csrf
        @method('put')
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Hotline Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Hotline Name" value="{{ $hotline->name }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="contact">Contact Number</label>
                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Hotline location" value="{{ $hotline->contact }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Update</button>
        </div>
    </form>
@stop
