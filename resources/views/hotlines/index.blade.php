@extends('layout')
@section('title', 'Hotline List')

@section('content')
    <h4>Hotlines</h4>
    <a class="btn btn-sm btn-info my-2" href="{{ route('hotlines.create') }}">Add Hotline Contact</a>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
        @if (count($hotlines))
            <table class="table table-light table-bordered table-striped">
                <thead>
                <tr>
                    <th>Hotline Name</th>
                    <th>Hotline Contact</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($hotlines as $hotline)
                    <tr>
                        <td>{{ $hotline->name }}</td>
                        <td>{{ $hotline->contact }}</td>
                        <td>
                            @csrf
                            {{--<a class="btn btn-sm btn-success" href="{{ route('hotlines.show', $hotline->id) }}">View</a>--}}
                            <a class="btn btn-sm btn-primary" href="{{ route('hotlines.edit', $hotline->id) }}">Edit</a>
                            <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $hotline->id }}">Delete</button>

                            {{--delete modal--}}
                            <div class="modal" id="delete{{ $hotline->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title text-center">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p>Are you sure you want to delete?</p>

                                            <form class="d-inline-block" method="post" action="{{ route('hotlines.destroy', $hotline->id) }}">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $hotlines->links() }}
        @else
            <div class="" align="center">
                <h4>No data!</h4>
            </div>
        @endif
    </div>
@stop
