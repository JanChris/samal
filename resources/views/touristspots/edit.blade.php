@extends('layout')

@section('title', 'Edit Event')

@section('content')
    <h2 class="my-3">Update Tourist Spot</h2><hr>
    @if ($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    @endif
    <form action="{{ route('touristspots.update', $touristspot->id) }}" method="post">
        @csrf
        @method('put')
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Tourist Spot Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Event Name" value="{{ $touristspot->name }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="category">Category</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Category" value="{{ $touristspot->category }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="location">Location</label>
                    <input type="text" class="form-control" id="location" name="location" placeholder="Location" value="{{ $touristspot->location }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Update</button>
            <button type="button" class="btn btn-outline-secondary">Cancel</button>
        </div>
    </form>
@stop

@section('script')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>
@stop
