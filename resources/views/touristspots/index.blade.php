@extends('layout')
@section('title', 'Tourist Spot List')

@section('content')
    <h4>Tourist Spot</h4>
    <a class="btn btn-sm btn-info my-2" href="{{ route('touristspots.create') }}">Add Tourist Spot</a>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <hr>
        @if (count($touristspots))
            <table class="table table-light table-bordered table-striped">
                <thead>
                <tr>
                    <th>Tourist Spot Name</th>
                    <th>Category</th>
                    <th>Location</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($touristspots as $touristspot)
                    <tr>
                        <td>{{ $touristspot->name }}</td>
                        <td>{{ $touristspot->category }}</td>
                        <td>{{ $touristspot->location }}</td>
                        <td>
                            @csrf
                            <a class="btn btn-sm btn-success" href="{{ route('touristspots.show', $touristspot->id) }}">View</a>
                            <a class="btn btn-sm btn-primary" href="{{ route('touristspots.edit', $touristspot->id) }}">Edit</a>
                            <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete{{ $touristspot->id }}">Delete</button>
                            {{--delete modal--}}
                            <div class="modal" id="delete{{ $touristspot->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header text-center">
                                            <h5 class="modal-title text-center">Delete Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <p>Are you sure you want to delete?</p>

                                            <form class="d-inline-block" method="post" action="{{ route('touristspots.destroy', $touristspot->id) }}">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $touristspots->links() }}
        @else
            <div class="" align="center">
                <h4>No data!</h4>
            </div>
        @endif
    </div>
@stop
