<?php

namespace App\Http\Controllers;

use App\Resort;
use Illuminate\Http\Request;

class ResortController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resorts = Resort::orderBy('updated_at', 'desc')->paginate(10);
        return view('resorts.index', ['resorts' => $resorts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resorts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|min:3',
            'contact'  => 'required',
            'location'  => 'required',
            'category'  => 'required',
            'description'  => 'required',
            'amenity'  => 'required',
        ]);

        Resort::create([
            'name'      => $request->name,
            'contact'  => $request->contact,
            'location'  => $request->location,
            'category'  => $request->category,
            'description'  => $request->description,
            'amenity'  => $request->amenity,
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('resorts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resort  $resort
     * @return \Illuminate\Http\Response
     */
    public function show(Resort $resort)
    {
        return view('resorts.show', ['resort' => $resort]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resort  $resort
     * @return \Illuminate\Http\Response
     */
    public function edit(Resort $resort)
    {
        return view('resorts.edit', ['resort' => $resort]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resort  $resort
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resort $resort)
    {
        $resort->name           = $request->name;
        $resort->contact        = $request->contact;
        $resort->location       = $request->location;
        $resort->amenity        = $request->amenity;
        $resort->category       = $request->category;
        $resort->description    = $request->description;
        $resort->save();

        session()->flash('message', 'Successfully Updated!!');

        return redirect(route('resorts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resort  $resort
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resort $resort)
    {
        $resort->delete();

        session()->flash('message', 'Successfully Deleted!!');

        return redirect(route('resorts.index'));
    }
}
