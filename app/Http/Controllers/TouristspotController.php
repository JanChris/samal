<?php

namespace App\Http\Controllers;

use App\Touristspot;
use Illuminate\Http\Request;

class TouristspotController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $touristspots = Touristspot::orderBy('updated_at', 'desc')->paginate(10);

        return view('touristspots.index', ['touristspots' => $touristspots]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('touristspots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|min:3',
            'category'  => 'required',
            'location'  => 'required',
        ]);

        Touristspot::create([
            'name'      => $request->name,
            'category'  => $request->category,
            'location'  => $request->location
        ]);

        session()->flash('message', 'Successfully Added!!');

        return redirect(route('touristspots.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Touristspot  $touristspot
     * @return \Illuminate\Http\Response
     */
    public function show(Touristspot $touristspot)
    {
        return view('touristspots.show', ['touristspot' => $touristspot]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Touristspot  $touristspot
     * @return \Illuminate\Http\Response
     */
    public function edit(Touristspot $touristspot)
    {
        return view('touristspots.edit', ['touristspot' => $touristspot]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Touristspot  $touristspot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Touristspot $touristspot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Touristspot  $touristspot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Touristspot $touristspot)
    {
        //
    }
}
